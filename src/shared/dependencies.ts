import { systemDateProvider } from './dateProvider';


const dateProvider = systemDateProvider;

export {
    dateProvider,
};
