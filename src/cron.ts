import { CronJob } from 'cron';
import { config } from 'dotenv';
config();
import { fetchPepJobs } from './schedulers/pepJobsScheduler';

const jobs = [
  {
    cronTime: '0 8 * * *', // every day at 8:00
    onTick: () => fetchPepJobs,
    isActive: process.env.FEATURE_FLAG_FETCH_PEP_JOBS,
    name: 'fetchPepJobs',
  }
];

let activeJobs = 0;
for (const job of jobs) {
  const cronjob = { timeZone: 'Europe/Paris', start: true, ...job };

  if (cronjob.isActive) {
    console.log(`🚀 The job "${cronjob.name}" is ON ${cronjob.cronTime}`);
    new CronJob(cronjob);
    activeJobs++;
  } else {
    console.log(`❌ The job "${cronjob.name}" is OFF`);
  }
}
console.log(`Started ${activeJobs} / ${jobs.length} cron jobs`);
