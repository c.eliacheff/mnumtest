import axios from 'axios';

import csv from 'csvtojson';


export async function fetchPepJobs() {
    try {
        let count = 0;
        csv({
                delimiter: ';',
            },
        ).fromStream(((await axios.get(process.env.PEP_ENDPOINT!, {
                responseType: 'stream',
            })).data),
        ).subscribe((_pepJob) => {
            return new Promise(async (resolve) => {
                // await usecases.updateLatestActivePepJobs(pepJob, { jobsService, dateProvider });
                count++;
                resolve();
            });
        }, () => {
        }, () => {
            console.log(count);
        });
    } catch (e) {
        console.log(e);
        throw new Error('Erreur lors de la récupération des offres de la pep');
    }
}
